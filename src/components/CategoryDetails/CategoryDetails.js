import React from "react";
import * as lodash from "lodash";
import css from "./CategoryDetails.module.css";
import numeral from 'numeral';

const CategoryDetails = ({ data }) => {
  const renderTableHeader = React.useCallback(() => {
    if (!data || !data.length) {
      return null;
    }

    let header = Object.keys(data[0]);

    return header.map((key, index) => {
      return (
        <th key={index} className={css.th}>
          {lodash.startCase(key)}
        </th>
      );
    });
  }, [data]);

  const renderTableData = React.useCallback(() => {
    return data.map((item) => {
      return (
        <tr key={item.id}>
          {Object.values(item).map((value) => (
            <td key={value} className={css.td}>
              {value}
            </td>
          ))}
        </tr>
      );
    });
  }, [data]);

  const [min, max] = React.useMemo(() => {
    const fundings = data && data.map((item) => item.fundingAmount);
    return [
      numeral(lodash.min(fundings)).format("($ 0.00 a)"),
      numeral(lodash.max(fundings)).format("($ 0.00 a)"),
    ];
  }, [data]);

  return data && data.length ? (
    <>
      <h2>
        Categories with funding range between {min} to {max}
      </h2>
      <table className={css.table}>
        <tbody>
          <tr>{renderTableHeader()}</tr>
          {renderTableData()}
        </tbody>
      </table>
    </>
  ) : null;
};

export default CategoryDetails;
