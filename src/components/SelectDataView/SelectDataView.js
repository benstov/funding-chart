import React from "react";
import Select from "react-select";

const SelectDataView = ({ onSelected, className, options }) => {

  const [selected, setSelected] = React.useState(options[0]);
  const handleChange = React.useCallback(
    (selected) => {
      setSelected(selected);
      // setValue({ selected: selected.value || null });
      return onSelected(selected.value);
    },
    [onSelected, setSelected]
  );

  return (
    <Select
      name="data-view-select"
      value={selected} // so here the default value of one will be set then updates during the on change event
      onChange={handleChange}
      options={options}
      className={className}
    />
  );
};

export default SelectDataView;
