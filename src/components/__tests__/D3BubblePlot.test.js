// import { shallow } from "enzyme";
import D3BubblePlot from "../D3BubblePlot/D3BubblePlot.js";
import ReactDOM from "react-dom";
import React from "react";
import { shallow, mount, render, configure } from "enzyme";
import renderer from "react-test-renderer";
import Adapter from "enzyme-adapter-react-16";

describe("D3BubblePlot", () => {
  it("renders correctly", () => {
    const div = document.createElement("div");
    ReactDOM.render(
      <D3BubblePlot data={[]} colors={(i) => i.toString()} />,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
    // On the first run of this test, Jest will generate a snapshot file automatically.
  });
  it("renders a snapshot with empty data", () => {
    const tree = renderer.create(<D3BubblePlot data={[]} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("renders a snapshot with no data", () => {
    const tree = renderer.create(<D3BubblePlot data={undefined} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("renders a snapshot with data example", () => {
    const tree = renderer
      .create(
        <D3BubblePlot
          data={[
            { cat: "a", id: "1" },
            { cat: "b", id: "2" },
            { cat: "c", id: "2" },
          ]}
        />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("renders a snapshot with data example", () => {
    const tree = renderer
      .create(
        <D3BubblePlot
          data={[
            { cat: "a", id: "1" },
            { cat: "b", id: "2" },
            { cat: "c", id: "2" },
          ]}
        />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders correct bubble", () => {
    configure({ adapter: new Adapter() });
    const wrapper = render(
      <D3BubblePlot
        data={[
          { category: "a", fundingAmount: 100, id: "1" },
          { category: "a", fundingAmount: 200, id: "2" },
          { category: "a", fundingAmount: 300, id: "3" },
          { category: "b", fundingAmount: 400, id: "4" },
        ]}
        colors={(i) => i.toString()}
        onSelect={() => {}}
      />
    );
    console.log(wrapper.text());
    expect(wrapper.text().includes("600")).toBe(true);
  });
});
