// import { shallow } from "enzyme";
import CategoryColorHelp from "../CategoryColorHelp/CategoryColorHelp.js";
import ReactDOM from "react-dom";
import React from "react";
import renderer from "react-test-renderer";

describe("CategoryColorHelp", () => {
  it("renders correctly", () => {
    const div = document.createElement("div");
    ReactDOM.render(
      <CategoryColorHelp data={[]} colors={(i) => i.toString()} />,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
    // On the first run of this test, Jest will generate a snapshot file automatically.
  });
  it("renders a snapshot with empty data", () => {
    const tree = renderer
      .create(<CategoryColorHelp data={[]} colors={(i) => i.toString()} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("renders a snapshot with no data", () => {
    const tree = renderer
      .create(<CategoryColorHelp colors={(i) => i.toString()} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
