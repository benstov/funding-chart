// import { shallow } from "enzyme";
import Chart from "../Chart/Chart.js";
import SelectDataView from "../SelectDataView/SelectDataView.js";
import ReactDOM from "react-dom";
import React from "react";

describe("Chart", () => {
  it("renders correctly", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Chart />, div);
    ReactDOM.unmountComponentAtNode(div);
    // On the first run of this test, Jest will generate a snapshot file automatically.
  });
});
