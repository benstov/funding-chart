// import { shallow } from "enzyme";
import CategoryDetails from "../CategoryDetails/CategoryDetails.js";
import ReactDOM from "react-dom";
import React from "react";
import renderer from "react-test-renderer";

describe("CategoryDetails", () => {
  it("renders correctly", () => {
    const div = document.createElement("div");
    ReactDOM.render(
      <CategoryDetails data={[]} colors={(i) => i.toString()} />,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
    // On the first run of this test, Jest will generate a snapshot file automatically.
  });
  it("renders a snapshot with empty data", () => {
    const tree = renderer.create(<CategoryDetails data={[]} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("renders a snapshot with no data", () => {
    const tree = renderer.create(<CategoryDetails data={undefined} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("renders a snapshot with data example", () => {
    const tree = renderer
      .create(<CategoryDetails data={[{ cat: "a", id: "1" }, { cat: "b", id: "2" }, { cat: "c", id: "3" }]} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
