import React, { useEffect, useRef, useMemo } from "react";
import * as d3 from "d3";
import * as lodash from "lodash";
import { aggregateCategoryAnalytics } from "../../utils";
import numeral from "numeral";
import css from "./D3BubblePlot.module.css";

const D3BubblePlot = ({
  data: rawData,
  onSelect,
  colors: circleColors,
  minBubbleSize = 20,
  maxBubbleSize = 40,
  dataViewMain = "fundingAmount", // can be "fundingAmount" / "count"
  dataViewSecondary = "count", // can be "fundingAmount" / "count"
}) => {
  // set the dimensions and margins of the graph
  //
  const data = useMemo(() => {
    return { children: aggregateCategoryAnalytics(rawData) };
  }, [rawData]);

  const ref = useRef();

  const handleCircleClick = React.useCallback(
    (d, i) => {
      // Add interactivity
      onSelect(d.data.category);
    },
    [onSelect]
  );
  const getFundingAmountLabel = React.useCallback(
    (fundingAmount) =>
      // Add interactivity
      numeral(fundingAmount).format("($ 0.00 a)"),
    []
  );

  // get not too big or not too small circle radius
  const getRByBoundry = React.useCallback(
    (r) =>
      // Add interactivity
      lodash.min([lodash.max([minBubbleSize, r / 2]), maxBubbleSize]),
    [minBubbleSize, maxBubbleSize]
  );

  // extract useEffect use forwardRef
  useEffect(() => {
    // Create Event Handlers for mouse
    var diameter = 600;
    var bubble = d3.pack(data).size([diameter, diameter]).padding(1.5);

    const margin = { top: 50, right: 20, bottom: 50, left: 100 },
      width = 800 - margin.left - margin.right,
      height = 400 - margin.top - margin.bottom;

    // clear chart
    d3.selectAll("svg > *").remove();

    // append the svg object to the ref that will contain the chart
    const svg = d3
      .select(ref.current)
      .attr("width", width)
      .attr("height", height + margin.top + margin.bottom)
      .style("margin", "0 auto")
      .style("display", "block")
      .style("padding", "1rem")
      .append("g")
      .attr("class", css.grid)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var nodes = d3.hierarchy(data).sum(function (d) {
      return d[dataViewMain];
    });

    // create side continues scale
    let yScale = d3
      .scaleLinear()
      .domain([
        d3.max(data.children, function (d) {
          return +d[dataViewSecondary];
        }),
        0,
      ])
      .range([0, height]);
    let yAxisGenerator = d3.axisLeft(yScale);
    yAxisGenerator.tickSize(-width).ticks(3);
    svg.append("g").call(yAxisGenerator);

    // select all nodes
    var node = svg
      .selectAll(".node")
      .data(bubble(nodes).descendants())
      .enter()
      .filter(function (d) {
        return !d.children;
      })
      .append("g")
      .attr("class", css.node)
      .attr("transform", function (d, i) {
        return (
          "translate(" + i + 50 + "," + yScale(d.data[dataViewSecondary]) + ")"
        );
      });

    // append circle
    node
      .append("circle")
      .attr("r", function (d) {
        return getRByBoundry(d.r);
      })
      .style("fill", function (d, i) {
        return circleColors(i);
      })
      .on("mouseover", function (d) {
        var r = d3.select(this).attr("r");
        const circle = d3.select(this);
        circle.attr("r", r * 1.2);
      })
      .on("mouseout", function (d) {
        var r = d3.select(this).attr("r");
        d3.select(this).attr("r", r / 1.2);
      })
      .on("click", handleCircleClick);

    node
      .append("text")
      .attr("dy", ".2em")
      .style("text-anchor", "middle")
      .text(function (d) {
        return dataViewMain === "fundingAmount"
          ? getFundingAmountLabel(d.data[dataViewMain])
          : d.data[dataViewMain];
        // return d.data.Name.substring(0, d.r / 3);
      })
      .attr("font-family", "sans-serif")
      .attr("font-size", function (d) {
        return getRByBoundry(d.r) / 2.5;
      })
      .attr("fill", "white")
      .on("click", handleCircleClick);
  }, [
    ref,
    data,
    onSelect,
    circleColors,
    handleCircleClick,
    getFundingAmountLabel,
    getRByBoundry,
    dataViewSecondary,
    dataViewMain,
  ]);

  return <svg ref={ref} />;
};

export default D3BubblePlot;
