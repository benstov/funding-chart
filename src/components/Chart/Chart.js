import React, { useState, useCallback } from "react";
import * as d3 from "d3";
import useFundingDataApi from "../../hooks/useFundingDataApi";

import D3BubblePlot from "../D3BubblePlot";
import CategoryDetails from "../CategoryDetails";
import CategoryColorHelp from "../CategoryColorHelp";
import css from "./Chart.module.css";
import SelectDataView from "../SelectDataView";

const Chart = () => {
  const [{ data, isLoading, isError }] = useFundingDataApi(
    "http://demo0377384.mockable.io/funding-test",
    []
  );

  const [selectedCategory, setSelectedCategory] = useState();
  const categoriesDetails = React.useMemo(() => {
    return data.filter((item) => item.category === selectedCategory);
  }, [selectedCategory, data]);

  const handleSelectCategory = useCallback((category) => {
    setSelectedCategory(category);
  }, []);
  const colors = React.useMemo(
    () => d3.scaleOrdinal().domain(data).range(d3.schemeSet2),
    [data]
  );

  // Map from main data view to the secondary data view
  // used to decide what to place as a secondary data view according to the key
  const mainToSecodary = React.useMemo(
    () =>
      new Map([
        ["fundingAmount", "count"], // main: fundingAmount, secondary: count
        ["count", "fundingAmount"], // main: count, secondary: fundingAmount
      ]),
    []
  );

  const handleSelected = React.useCallback((selectedOption) => {
    setSelectedDataView(selectedOption);
  }, []);

  // drop down options
  const options = React.useMemo(
    () => [
      { value: "fundingAmount", label: "Funding Amount" },
      { value: "count", label: "Count" },
    ],
    []
  );

  const [selectedDataView, setSelectedDataView] = React.useState(
    options[0].value
  );
  return isLoading ? (
    <div>Loading...</div>
  ) : !data || !data.length || isError ? (
    <div>Error occurred while fetching data</div>
  ) : (
    <>
      <SelectDataView
        options={options} // [count/fundingAmount]
        selected={selectedDataView} // count / fundingAmount
        onSelected={handleSelected}
        className={css.select}
      ></SelectDataView>
      <CategoryColorHelp data={data} colors={colors} />

      <D3BubblePlot
        dataViewMain={selectedDataView}
        dataViewSecondary={mainToSecodary.get(selectedDataView)}
        data={data}
        onSelect={handleSelectCategory}
        colors={colors}
      />
      {categoriesDetails ? <CategoryDetails data={categoriesDetails} /> : null}
      <div>N = {data.length}</div>
    </>
  );
};

export default Chart;
