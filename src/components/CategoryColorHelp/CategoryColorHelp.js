import React from "react";
import css from "./CategoryColorHelp.module.css";
import * as lodash from "lodash";

const Circle = ({ color }) => {
  const circleStyle = {
    padding: 4,
    marginRight: 10,
    display: "inline-block",
    borderRadius: "50%",
    width: 4,
    height: 4,
    backgroundColor: color,
  };
  return <div style={circleStyle}></div>;
};

const CategoryColorHelp = ({ data, colors }) => {
  const categories = React.useMemo(() => {
    const categories =
      data && data.length ? lodash.uniqBy(data.map((d) => d.category)) : [];
    return categories;
  }, [data]);

  return data && data.length ? (
    <div className={css.container}>
      {categories.map((category, i) => (
        <div className={css.wrapper} key={category}>
          <Circle color={colors(i)} /> {category}
        </div>
      ))}
    </div>
  ) : null;
};

export default CategoryColorHelp;
