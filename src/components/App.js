import React from "react";
import css from "./App.module.css";
import Chart from "./Chart";

function App() {
  return (
    <div className={css.app}>
      <h1>Funding by Industry Analytics</h1>
      <Chart></Chart>
    </div>
  );
}

export default App;
