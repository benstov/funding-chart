import React, { useState, useEffect, useReducer } from "react";
import axios from "axios";

const ActionTypes = Object.freeze({
  FETCH_INIT: "FETCH_INIT",
  FETCH_SUCCESS: "FETCH_SUCCESS",
  FETCH_FAILURE: "FETCH_FAILURE",
});

const dataFetchReducer = (state, action) => {
  switch (action.type) {
    case ActionTypes.FETCH_INIT:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case ActionTypes.FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.payload,
      };
    case ActionTypes.FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      throw new Error();
  }
};

// hook for executing call and receiving request state
const useFundingDataApi = (initialUrl, initialData) => {
  const [url, setUrl] = useState(initialUrl);
  const [state, dispatch] = useReducer(dataFetchReducer, {
    isLoading: false,
    isError: false,
    data: initialData,
  });

  useEffect(() => {
    let didCancel = false;
    const fetchData = async () => {
      dispatch({ type: ActionTypes.FETCH_INIT });
      try {
        const result = await axios(url);
        if (!didCancel) {
          dispatch({ type: ActionTypes.FETCH_SUCCESS, payload: result.data });
        }
      } catch (error) {
        if (!didCancel) {
          dispatch({ type: ActionTypes.FETCH_FAILURE });
        }
      }
    };

    fetchData();
    return () => {
      didCancel = true;
    };
  }, [url]);

  // return setUrl in case user would like to query with filtering options

  return [state, setUrl];
};

export default useFundingDataApi;
