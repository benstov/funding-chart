import { tsvParse, csvParse } from "d3-dsv";
import { timeParse } from "d3-time-format";
import * as lodash from "lodash";

// group by category
// return:
// {
//  category: category name
//  count: 
//  fundingAmount: lodash.sumBy(groupedByCategory[key], "fundingAmount"),
// }
export function aggregateCategoryAnalytics(data) {
  const groupedByCategory = lodash.groupBy(data, "category");
  return lodash.map(lodash.keys(groupedByCategory), function (key) {
    return {
      category: key,
      count: groupedByCategory[key].length,
      fundingAmount: lodash.sumBy(groupedByCategory[key], "fundingAmount"),
    };
  });
}
